#!/usr/bin/env ruby
# coding: utf-8
# vim: set fileencoding=utf-8 :
#
# server.rb
# 
# Author:   Makoto Shimazu <makoto.shimaz@gmail.com>
# URL:      https://amiq11.tumblr.com               
# License:  2-Clause BSD License                    
# Created:  2015-12-20                              
#
#
# Copyright (c) 2015, Makoto Shimazu
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'active_record'
require 'digest'
require 'sinatra'
require 'sinatra/reloader'

class Christmas2015App < Sinatra::Base
  use Rack::Session::Pool,
      path: '/',
      domain: nil,
      expire_after: 60 * 60, # Cookieの有効期限を60分に
      secret: Digest::SHA256.hexdigest(rand.to_s)

  ActiveRecord::Base.configurations = YAML.load_file('database.yml')
  ActiveRecord::Base.establish_connection(:development)


  class User < ActiveRecord::Base
  end

  get '/' do
    is_failed = (params['failed'] != nil) ? true : false
    erb :index, locals: {fail_to_login: is_failed}
  end

  get '/logged_in' do
    if session[:user_id] == nil
      redirect '/?failed=1'
    else
      session[:login_count] += 1
      #     name = User.find(session[:user_id]).name
      name = session[:user_name]
      erb :login, locals: {username: name, new_login: (session[:login_count] == 1)}
    end
  end

  get '/logout' do
    session.clear
    redirect '/'
  end

  post '/login' do
    redirect '/logged_in' if session[:user_id] != nil

    pass = Digest::MD5.hexdigest(params['pass'])

    # user = User.where(name: params['user'], pass: pass).first と書けば安全
    con = ActiveRecord::Base.connection
    user = con.select_all("select id, name from users where name='#{params["user"]}' and pass='#{pass}'").first
    p user
    if user
      session.clear
      session[:logged_in_at] = Time.now()
      session[:user_id] = user["id"]
      session[:user_name] = user["name"]
      session[:login_count] = 0
      p session
      redirect '/logged_in'
    else
      redirect '/?failed=1'
    end
  end
end

Christmas2015: SQLi
====

How to execute the problem
----

1. Install Docker (1.7.1 or later)
2. Install docker-compose
3. Clone this repository
4. Build base image
5. Build problem image
6. Try!

Install Docker (1.7.1 or later)
----

Reference: [Ubuntu](https://docs.docker.com/engine/installation/ubuntulinux/)

Please do not forget to add you into `docker` group. The following command is to do.

```
$ sudo usermod -aG docker YOUR_USER_NAME
```

After this, you should log-out and log-in again.

Install Docker Compose
----

Reference: [Install Docker Compose](https://docs.docker.com/compose/install/)

Clone this repository
---

```
$ git clone https://MakotoShimazu@bitbucket.org/MakotoShimazu/christmas2015.git
```

Build base image
----

```
$ cd christmas2015
$ docker-compose build base
```

This will take some time, so let's take some coffee.

Build and start this problem
----

```
$ docker-compose build problem
$ docker-compose up problem
```

Try!
----

If you use Google Chrome, type this command:

```
$ google-chrome http://localhost:14567
```


When you want to start again after this build process, you can try to start by the following:

```
$ docker-compose up problem
```



Settings
----

- Example user
  - ID: admin
  - Pass: hogehoge
- DB
  - kind: sqlite
  - place: ./problem/database.db

Please modify problem/database.db directly!:)